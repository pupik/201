# Tweets count

## Description

This application allows to read Tweeter Data from Kafka then:
 1) groups it by hash-tags and country code
 2) calculates count
 3) stores calculated data in HDFS partitioned by day and hour

When there is a message(s) from Kafka, which intersects with partitions, that already stored in HDFS, data will be updated

## Configuration

Application works with parameters that should be passed as json file:

```
{
	"kafkaConfig":{
		"subscribe":"topic",
		"startingOffsets":"earliest",
		"endingOffsets":"latest",
		"enable.auto.commit":"false",
		"kafka.bootstrap.servers":"localhost:9092"
	},
	"fsConfig":{
		"outputPath":"path"
	}
}
```
In kafkaConfig section, params that are noticed in spark 2.3.0 could be passed

## Example of usage

```
spark-submit --master yarn \
--packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0 \
--master yarn TweetsCount-1.0-SNAPSHOT-jar-with-dependencies.jar conf.json
```