package com.epam.tweets.count

import com.epam.tweets.count.config.{AppProperties, Config}
import org.apache.spark.sql.SparkSession
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SparkTweetsProcessorTest extends FunSuite {

  test("test initializeSparkSession") {
    val kafkaConfig: Map[String, String] = Map("par1" -> "val1", "par2" -> "val2");
    val fsConfig: Map[String, String] = Map("outputPath" -> "path");
    var config = new Config(kafkaConfig, fsConfig);

    val processor = new SparkTweetsProcessor(
      path = config.fsConfig.get("outputPath").get,
      kafkaConfig = config.kafkaConfig
    );


    val spark = processor.initializeSparkSession()
    assert(spark.isInstanceOf[SparkSession])
    assert(spark.sparkContext.getConf.get("spark.master") === "local[*]")

    val resultConf = spark.sparkContext.hadoopConfiguration
    assert(resultConf.get(AppProperties.FS_HDFS_IMPL) === classOf[org.apache.hadoop.hdfs.DistributedFileSystem].getName)
    assert(resultConf.get(AppProperties.FS_FILE_IMPL) === classOf[org.apache.hadoop.fs.LocalFileSystem].getName)
  }
}
