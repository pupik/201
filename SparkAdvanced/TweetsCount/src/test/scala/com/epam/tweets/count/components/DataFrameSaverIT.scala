package com.epam.tweets.count.components

import net.manub.embeddedkafka.{EmbeddedKafka, EmbeddedKafkaConfig}
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import org.scalatest.{Matchers, WordSpec}
import twitter4j.{Status, TwitterObjectFactory}

class DataFrameSaverIT extends WordSpec with Matchers with EmbeddedKafka with SparkSessionSpec {

  "Run data save process: read kafka data, try to save it, should work without exceptions" should {
    val readKafkaData = new KafkaDataReader()
    val kafkaConfig: Map[String, String] = Map("subscribe" -> "testtest", "kafka.bootstrap.servers" -> "localhost:9999")
    val path = getClass.getResource("/df").getPath

    "work" in {
      implicit val config = EmbeddedKafkaConfig(kafkaPort = 9999)
      implicit val serializer = new StringSerializer()

      withRunningKafka {
        createCustomTopic("testtest")
        val status =
          TwitterObjectFactory.createObject(
            "{\"text\":\"Some text\",\"created_at\":\"Thu Feb 21 15:13:12 +0000 2019\",\"" +
              "place\":{\"country_code\":\"KZ\"}," +
              "\"entities\":{\"hashtags\":[{\"text\":\"ABR\",\"indices\":[105,116]}]}}")
            .asInstanceOf[Status]

        val producerRecord = new ProducerRecord("testtest", "TEST", TwitterObjectFactory.getRawJSON(status))

        publishToKafka(producerRecord)

        val result = readKafkaData(spark, kafkaConfig)
        val saver = new DataFrameSaver()

        // save the result, if there is no exception, it means that it is saved properly
        val saveData = saver(result, path)

        // delete saved data
        result.unpersist()
      }
    }
  }
}
