package com.epam.tweets.count.config

object AppProperties {

  val PATH_LABEL = "path"
  val OUTPUT_PATH = "outputPath"

  val PARQUET_FORMAT = "parquet"
  val KAFKA_FORMAT = "kafka"

  val FS_HDFS_IMPL = "fs.hdfs.impl"
  val FS_FILE_IMPL = "fs.file.impl"
  val PARTITION_OVERWRITE_MODE = "spark.sql.sources.partitionOverwriteMode"
  val PARTITION_MODE_DYNAMIC = "dynamic"

  val COMPRESSION = "compression"
  val GZIP = "gzip"

  val KAFKA_VALUE = "value"
  val TWEET_DATE_FORMAT = "EEE MMM dd HH:mm:ss zzz yyyy"

  val COLUMN_DATE = "date"
  val COLUMN_HOUR = "hour"
  val COLUMN_HASHTAGS = "hashtags"
  val COLUMN_COUNTRY_CODE = "countryCode"
  val COLUMN_COUNT = "count"
}
