package com.epam.tweets.count

import com.epam.tweets.count.components.{DataFrameSaver, DataMerger, KafkaDataReader, SavedDataReader}
import com.epam.tweets.count.config.AppProperties
import za.co.absa.spline.core.SparkLineageInitializer._
import org.apache.spark.sql._

class SparkTweetsProcessor(
                            readSavedData: SavedDataReader = new SavedDataReader,
                            readKafkaData: KafkaDataReader = new KafkaDataReader,
                            mergeData: DataMerger = new DataMerger,
                            saveDataFrame: DataFrameSaver = new DataFrameSaver,
                            path: String,
                            kafkaConfig: Map[String, String]
                          ) {
  /**
    * The following steps are executing, when using run method
    * - read data from HDFS from previous execution
    * - read kafka data and extract necessary data
    * - merge kafka data with data from previous execution
    * - save dataframe to hdfs
    */
  def run(): Unit = {

    val spark = initializeSparkSession()

    val oldDataDf = readSavedData(spark, path)

    val processedDf = readKafkaData(spark, kafkaConfig)

    val mergedDf = mergeData(oldDataDf, processedDf)

    saveDataFrame(mergedDf, path)
  }

  /**
    * Function to init and return spark session, the following steps are made:
    * - creation of sparkSession with spark.sql.sources.partitionOverwriteMode dynamic parameter to overwrite existing hdfs data
    * - apply to spark context hdfs parameters to work with hadoop
    *
    * @param hadoopConfig
    * @return sparkSession with local[*]
    */
  def initializeSparkSession(): SparkSession = {
    val spark = SparkSession
      .builder()
      .config(AppProperties.PARTITION_OVERWRITE_MODE, AppProperties.PARTITION_MODE_DYNAMIC)
      .config("spark.testing.memory", "2147480000")
      .getOrCreate()

    // Data lineage of the job will be captured and stored in the
    // configured Mongo database for further visualization by Spline Web UI
    spark.enableLineageTracking()

    spark.sparkContext.hadoopConfiguration.set(AppProperties.FS_HDFS_IMPL, classOf[org.apache.hadoop.hdfs.DistributedFileSystem].getName)
    spark.sparkContext.hadoopConfiguration.set(AppProperties.FS_FILE_IMPL, classOf[org.apache.hadoop.fs.LocalFileSystem].getName)

    spark
  }
}
