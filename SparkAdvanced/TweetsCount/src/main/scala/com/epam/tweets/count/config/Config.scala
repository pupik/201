package com.epam.tweets.count.config

class Config(val kafkaConfig: Map[String, String],
             val fsConfig: Map[String, String])
