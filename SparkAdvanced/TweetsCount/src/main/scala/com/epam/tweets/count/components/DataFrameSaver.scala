package com.epam.tweets.count.components

import com.epam.tweets.count.config.AppProperties
import org.apache.spark.sql.{DataFrame, SaveMode}

/**
  * Component is responsible for storing data in HDFS
  * partitioned by date and hor column w
  * ith GZIP compression in parquet format
  *
  */
class DataFrameSaver() {

  def apply(dfToSave: DataFrame, path: String): Unit = {
    dfToSave
      .write
        .partitionBy(AppProperties.COLUMN_DATE, AppProperties.COLUMN_HOUR)
        .mode(SaveMode.Overwrite)
        .format(AppProperties.PARQUET_FORMAT)
        .option(AppProperties.PATH_LABEL, path)
        .option(AppProperties.COMPRESSION, AppProperties.GZIP)
      .save()
  }
}
