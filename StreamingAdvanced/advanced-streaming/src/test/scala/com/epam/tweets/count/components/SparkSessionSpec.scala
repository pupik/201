package com.epam.tweets.count.components

import com.epam.tweets.count.config.AppProperties
import org.apache.spark.sql.SparkSession

trait SparkSessionSpec {
  val spark = SparkSession
    .builder()
    .master("local[*]")
    .config(AppProperties.PARTITION_OVERWRITE_MODE, AppProperties.PARTITION_MODE_DYNAMIC)
    .config("spark.ui.enabled", false)
    .config("spark.testing.memory", "2147480000")
    .getOrCreate()
  spark.sparkContext.hadoopConfiguration.set(AppProperties.FS_HDFS_IMPL, classOf[org.apache.hadoop.hdfs.DistributedFileSystem].getName)
  spark.sparkContext.hadoopConfiguration.set(AppProperties.FS_FILE_IMPL, classOf[org.apache.hadoop.fs.LocalFileSystem].getName)
}
