package com.epam.tweets.count.components

import net.manub.embeddedkafka.{EmbeddedKafka, EmbeddedKafkaConfig}
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import org.scalatest.{Matchers, WordSpec}
import twitter4j.{Status, TwitterObjectFactory}

class DataMergerIT extends WordSpec with Matchers with EmbeddedKafka with SparkSessionSpec {

  "Run merge process: read kafka data, try to merge it, should work properly" should {
    val readKafkaData = new KafkaDataReader()
    val kafkaConfig: Map[String, String] = Map("subscribe" -> "testtest", "kafka.bootstrap.servers" -> "localhost:9999")

    "work" in {
      implicit val config = EmbeddedKafkaConfig(kafkaPort = 9999)
      implicit val serializer = new StringSerializer()

      withRunningKafka {
        createCustomTopic("testtest")
        val status =
          TwitterObjectFactory.createObject(
            "{\"text\":\"Some text\",\"created_at\":\"Thu Feb 21 15:13:12 +0000 2019\",\"" +
              "place\":{\"country_code\":\"KZ\"}," +
              "\"entities\":{\"hashtags\":[{\"text\":\"ABR\",\"indices\":[105,116]}]}}")
            .asInstanceOf[Status]

        val producerRecord = new ProducerRecord("testtest", "TEST", TwitterObjectFactory.getRawJSON(status))

        publishToKafka(producerRecord)

        val result = readKafkaData(spark, kafkaConfig)
        val merger = new DataMerger()
        val mergedData1 = merger(null, result)
        val mergedData2 = merger(result, result)

        // should return the same, because first parameter is null
        assert(mergedData1 == result)
        // should merge, and count should be 2, because we merge 2 equal dataframes
        assert(mergedData2.collectAsList().get(0).toString() == "[2019-02-21,18,abr,KZ,2]")
      }
    }
  }
}
