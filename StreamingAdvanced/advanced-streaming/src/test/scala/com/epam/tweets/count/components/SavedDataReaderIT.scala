package com.epam.tweets.count.components

import org.scalatest.{BeforeAndAfterAll, FunSuite}

class SavedDataReaderIT extends FunSuite with BeforeAndAfterAll with SparkSessionSpec {

  test("Read saved data with wrong path: should work properly") {
    val reader = new SavedDataReader()

    val result = reader(spark, "Some path")

    assert(result == null)
  }

  test("Read data with correct path: should work properly") {
    val reader = new SavedDataReader()
    val path = getClass.getResource("/dir").getPath

    val result = reader(spark, getClass.getResource("/dir").getPath.replaceFirst("/", ""))

    assert(result != null)
  }
}
