package com.epam.tweets.count.components

import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * Component is responsible for reading data stored in HDFS from specified path
  *
  */
class SavedDataReader() {

  def apply(spark: SparkSession, path: String): DataFrame = {
    val hadoopFs = FileSystem.get(spark.sparkContext.hadoopConfiguration)
    val hadoopPath = new Path(path)

    if (hadoopFs.exists(hadoopPath) &&
      hadoopFs.getFileStatus(hadoopPath).isDirectory &&
      !hadoopFs.listStatus(hadoopPath).isEmpty) {

      spark
        .read
        .parquet(path)

    } else {
      null
    }
  }
}
