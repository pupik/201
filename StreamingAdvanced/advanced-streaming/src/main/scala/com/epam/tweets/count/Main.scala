package com.epam.tweets.count

import com.epam.tweets.count.config.{AppProperties, Config}
import org.json4s.DefaultFormats
import org.json4s.jackson.Serialization.read

import scala.io.Source

/**
  * Application entry point
  * args should contain path to json configuration file
  * example: C:\folder\conf.json
  */
object Main {

  def main(args: Array[String]): Unit = {
    if (args.isEmpty) {
      throw new IllegalArgumentException("Arg 0 should contain path to json configuration")
    }

    val config = readConfig(args.head)
    val processor = new SparkTweetsProcessor(
      path = config.fsConfig.get(AppProperties.OUTPUT_PATH).get,
      kafkaConfig = config.kafkaConfig
    );

    processor.run()
  }

  /**
    * Method to read json file and extracting necessary configuration to Config class
    *
    * @param filename
    * @return
    */
  def readConfig(filename: String): Config = {
    val json = Source.fromFile(filename)

    implicit val formats = DefaultFormats
    var config = read[Config](json.reader())
    if (config.fsConfig == null || config.fsConfig.get(AppProperties.OUTPUT_PATH) == null) {
      throw new IllegalArgumentException("outputPath in fsConfig property should be specified")
    }
    if (config.kafkaConfig == null) {
      throw new IllegalArgumentException("kafkaConfig property should be specified")
    }

    config
  }
}
