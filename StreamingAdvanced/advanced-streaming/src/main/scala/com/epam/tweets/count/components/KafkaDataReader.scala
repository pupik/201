package com.epam.tweets.count.components

import com.epam.tweets.count.config.AppProperties
import org.apache.spark.sql.functions.{regexp_replace, _}
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

/**
  * Component is responsible for read data from kafka and transform it by specified rules
  *
  */
class KafkaDataReader() {

  /**
    * The following steps are executing:
    * - read kafka data from topic in config
    * - extract necessary fields from kafka value object
    * - group and count by necessary fields: date, hour, hashtags and countryCode
    *
    * @param spark
    * @param kafkaConfig
    * @return dataFrame with grouping and counting necessary parameters
    */
  def apply(spark: SparkSession, kafkaConfig: Map[String, String]): DataFrame = {

    import spark.implicits._
    val kafkaDf = spark
      .readStream
      .format(AppProperties.KAFKA_FORMAT)
      .options(kafkaConfig)
      .load()
      .select(column(AppProperties.KAFKA_VALUE).cast(to = StringType))
      .select(
        get_json_object(column(AppProperties.KAFKA_VALUE), "$.created_at") as AppProperties.COLUMN_DATE,
        get_json_object(column(AppProperties.KAFKA_VALUE), "$.place.country_code") as AppProperties.COLUMN_COUNTRY_CODE,
        hashTags as AppProperties.COLUMN_HASHTAGS
      ).where(
        column(AppProperties.COLUMN_DATE).isNotNull &&
        column(AppProperties.COLUMN_HASHTAGS).isNotNull
      )

    val processedDf =
      kafkaDf.select(
        hour(to_timestamp(column(AppProperties.COLUMN_DATE), AppProperties.TWEET_DATE_FORMAT)) as AppProperties.COLUMN_HOUR,
        to_date(column(AppProperties.COLUMN_DATE), AppProperties.TWEET_DATE_FORMAT) as AppProperties.COLUMN_DATE,
        explode(column(AppProperties.COLUMN_HASHTAGS)) as AppProperties.COLUMN_HASHTAGS,
        column(AppProperties.COLUMN_COUNTRY_CODE) as AppProperties.COLUMN_COUNTRY_CODE
      )

    processedDf
      .withWatermark("timestamp", "59 seconds") //set watermark using timestamp filed with a max delay of 3s.
      .groupBy(
      window($"timestamp", "60 minutes", "1 minute"),
      column(AppProperties.COLUMN_DATE),
      column(AppProperties.COLUMN_HOUR),
      lower(column(AppProperties.COLUMN_HASHTAGS)) as AppProperties.COLUMN_HASHTAGS,
      column(AppProperties.COLUMN_COUNTRY_CODE)
    ).count()
  }

  /**
    * This method is responsible for transformation of hashTags string column to hashTags array column
    *
    * @return column with array of hashTags
    */
  private def hashTags: Column = {

    val hashTagsString = get_json_object(column(AppProperties.KAFKA_VALUE), "$.entities.hashtags[*].text")
    val jsonArraySigns = """\[|"|\]"""
    val hashTagsCleaned = regexp_replace(hashTagsString, jsonArraySigns, "")
    split(hashTagsCleaned, ",")
  }
}
