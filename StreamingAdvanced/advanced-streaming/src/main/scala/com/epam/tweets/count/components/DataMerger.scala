package com.epam.tweets.count.components

import com.epam.tweets.count.config.AppProperties
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{column, lower, sum}

/**
  * Component is responsible for merging old stored data
  * with new data from kafka
  *
  */
class DataMerger() {

  def apply(oldData: DataFrame, newData: DataFrame): DataFrame = {
    if (oldData != null) {
      newData.unionByName(oldData)
        .groupBy(
          column(AppProperties.COLUMN_DATE),
          column(AppProperties.COLUMN_HOUR),
          lower(column(AppProperties.COLUMN_HASHTAGS)) as AppProperties.COLUMN_HASHTAGS,
          column(AppProperties.COLUMN_COUNTRY_CODE)
        ).agg(sum(AppProperties.COLUMN_COUNT) as AppProperties.COLUMN_COUNT)
    } else {
      newData
    }
  }
}
