DROP VIEW IF EXISTS top_device;
CREATE view top_device AS
    WITH device_count AS
	(
		SELECT city_id, device, COUNT(*) AS counter
		FROM logs_filtered
		GROUP BY city_id, device
	)
	SELECT device_count.city_id, device_count.device, device_count.counter
	FROM device_count
	JOIN
	(
		SELECT city_id, MAX( counter ) AS maxCounter
		FROM device_count
		GROUP BY city_id
	) device_max_count
	ON device_max_count.maxCounter = device_count.counter AND device_max_count.city_id=device_count.city_id;

DROP VIEW IF EXISTS top_browser;
CREATE view top_browser AS
   WITH browser_count AS
   	(
   		SELECT city_id, browser, COUNT(*) AS counter
   		FROM logs_filtered
   		GROUP BY city_id, browser
   	)
   	SELECT browser_count.city_id, browser_count.browser, browser_count.counter
   	FROM browser_count
   	JOIN
   	(
   		SELECT city_id, MAX( counter ) AS maxCounter
   		FROM browser_count
   		GROUP BY city_id
   	) browser_max_count
   	ON browser_max_count.maxCounter = browser_count.counter AND browser_max_count.city_id = browser_count.city_id;

DROP VIEW IF EXISTS top_os;
CREATE view top_os AS
	WITH os_count AS
	(
		SELECT city_id, os, COUNT(*) AS counter
		FROM logs_filtered
		GROUP BY city_id, os
	)
	SELECT os_count.city_id, os_count.os, os_count.counter
	FROM os_count
	JOIN
	(
		SELECT city_id, MAX( counter ) AS maxCounter
		FROM os_count
		GROUP BY city_id
	) os_max_count
	ON os_max_count.maxCounter = os_count.counter AND os_max_count.city_id = os_count.city_id;

SELECT city.id, city.name, b.browser, o.os, d.device
FROM cities city
JOIN top_browser b ON b.city_id = city.id
JOIN top_os o ON o.city_id = city.id
JOIN top_device d ON d.city_id = city.id ORDER BY city.id;