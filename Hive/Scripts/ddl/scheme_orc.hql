-- Register UDTF function for parse user-agent data
ADD jar ${hiveconf:udtf_path};
CREATE TEMPORARY FUNCTION user_agent_parse AS '${hiveconf:udtf_main_class}';

-- Create orc table with necessary columns for fast querying in views
DROP TABLE IF EXISTS logs_filtered;
CREATE TABLE logs_filtered (
device string, os string, browser string, city_id bigint
) STORED AS ORC tblproperties ('orc.compress' = 'SNAPPY');

-- Insert necessary data to orc table
INSERT INTO TABLE logs_filtered
SELECT uap.device, uap.os, uap.browser, city_id FROM logs
LATERAL VIEW user_agent_parse(user_agent) uap AS device, os, browser;