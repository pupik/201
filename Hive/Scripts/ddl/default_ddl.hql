-- Main logs table with raw data from files
DROP TABLE IF EXISTS logs;
CREATE EXTERNAL TABLE logs(
    bid_id string,
    time_stamp string,
    ipinyou_id string,
    user_agent string,
    ip string,
    region_id string,
    city_id bigint,
    ad_exchange string,
    domain string,
    url string,
    anonymous_url string,
    ads_lot_id string,
    ads_lot_width string,
    ads_lot_height string,
    ads_lot_visibility string,
    ads_lot_format string,
    ads_lot_floor_price string,
    creative_id string,
    bidding_price string,
    paying_price string,
    landing_page_url string,
    advertiser_id string,
    user_profile_ids string
    )
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 
    'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 
    'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
    '${hiveconf:logs_path}';

-- City reference from file
DROP TABLE IF EXISTS cities;
CREATE EXTERNAL TABLE cities(
    id bigint,
    name string
    )
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT
    'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
    'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
    '${hiveconf:city_path}';

-- Register UDTF function for parse user-agent data
ADD jar ${hiveconf:udtf_path};
CREATE TEMPORARY FUNCTION user_agent_parse AS '${hiveconf:udtf_main_class}';

-- Create table of parsed data with city_id interaction
DROP TABLE IF EXISTS logs_filtered;
CREATE TABLE logs_filtered (
device string, os string, browser string, city_id bigint);

-- Insert necessary data to filtered table
INSERT INTO TABLE logs_filtered SELECT uap.device, uap.os, uap.browser, city_id FROM logs
LATERAL VIEW user_agent_parse(user_agent) uap AS device, os, browser;