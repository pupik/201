SET hive.optimize.sort.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;

-- Register UDTF function for parse user-agent data
ADD jar ${hiveconf:udtf_path};
CREATE TEMPORARY FUNCTION user_agent_parse AS '${hiveconf:udtf_main_class}';

-- Create table of parsed data with city_id interaction
DROP TABLE IF EXISTS logs_filtered;
CREATE TABLE logs_filtered (device string, os string, browser string)
PARTITIONED BY(city_id bigint);

-- Insert necessary data to orc table
INSERT INTO TABLE logs_filtered
PARTITION (city_id)
SELECT uap.device, uap.os, uap.browser, city_id FROM logs
LATERAL VIEW user_agent_parse(user_agent) uap AS device, os, browser;
