DROP INDEX IF EXISTS logs_filtered_index ON logs_filtered;

CREATE INDEX logs_filtered_index
ON TABLE logs_filtered (city_id)
AS 'org.apache.hadoop.hive.ql.index.compact.CompactIndexHandler'
WITH DEFERRED REBUILD;

ALTER INDEX logs_filtered_index ON logs_filtered REBUILD;