#!/usr/bin/env bash

logs_dir=/ipinyou/logs
report_dir=/ipinyou/report
logs_path='hdfs://sandbox-hdp.hortonworks.com:8020/ipintoyou/*.*.bz2'
city_path='hdfs://sandbox-hdp.hortonworks.com:8020/ipintoyou/city'
udtf_path='hdfs://sandbox-hdp.hortonworks.com:8020/udf/udf-1.0-SNAPSHOT-all.jar'
udtf_main_class='com.epam.UserAgentUDTF'
query_hql='query/query.hql'

mkdir ${logs_dir}
mkdir ${report_dir}

write_log_for_report () {
    init_script=$1
    query_name=$2
    times=$3
    for (( i=1; i<= ${times}; i++ ))
    do
		echo "${i} of ${times} runnning"
        log="${logs_dir}/${query_name}_${i}"
        hive -v --hiveconf udtf_path=${udtf_path} --hiveconf udtf_main_class=${udtf_main_class} -i ${init_script} -f ${query_hql} &> ${log};
        taken=$( tail -n1 ${log} | grep -oP "[0-9]+\.[0-9]+" )
        echo "${i},${query_name},${taken}" >> ${report_dir}/result.csv
    done
}

#initial script
echo "initial script"
hive -v --hiveconf logs_path=${logs_path} --hiveconf city_path=${city_path} --hiveconf udtf_path=${udtf_path} --hiveconf udtf_main_class=${udtf_main_class} -f ddl/default_ddl.hql;

#default scheme
echo "default scheme on mr"
write_log_for_report 'techniques/technique_mr.hql' default_scheme 5

#first scheme
echo "first partition scheme"
write_log_for_report 'ddl/scheme_1.hql' first_scheme 5

#second scheme
echo "second partition scheme"
write_log_for_report 'ddl/scheme_2.hql' second_scheme 5

#third scheme
echo "third partition scheme"
write_log_for_report 'ddl/scheme_3.hql' third_scheme 5

#fourth scheme
echo "fourth partition scheme"
write_log_for_report 'ddl/scheme_4.hql' fourth_scheme 5

#orc scheme
echo "orc scheme"
write_log_for_report 'ddl/scheme_orc.hql' orc_scheme 5

#orc scheme with index
echo "orc scheme with index"
write_log_for_report 'ddl/index_creation.hql' orc_indexed_scheme 5

#tez vectorization technique
echo "orc with vectorization scheme"
write_log_for_report 'techniques/technique_vectorization.hql' vectorization_scheme 5

#tez technique
echo "orc with tez scheme"
write_log_for_report 'techniques/technique_tez.hql' tez_scheme 5