package com.epam;

import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StandardStructObjectInspector;

import java.util.List;

/**
 * Test object inspector for testing
 */
public class TestStandardStructObjectInspector extends StandardStructObjectInspector {

    public TestStandardStructObjectInspector() {
        super();
    }

    public TestStandardStructObjectInspector(List<String> structFieldNames, List<ObjectInspector> structFieldObjectInspectors) {
        super(structFieldNames, structFieldObjectInspectors);
    }
}
