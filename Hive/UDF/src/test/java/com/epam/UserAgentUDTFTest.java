package com.epam;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.Collector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ThriftUnionObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.JavaStringObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserAgentUDTFTest {

    @InjectMocks
    UserAgentUDTF userAgentUDTF;

    @Mock
    Collector collector;

    @Test
    public void initialize_shouldProcessOk() throws UDFArgumentException {

        UserAgentUDTF userAgentUDTF = new UserAgentUDTF();
        List<String> structFieldNames = Arrays.asList(String.valueOf("Name1"));
        List<ObjectInspector> structFieldObjectInspectors =
                Arrays.asList(PrimitiveObjectInspectorFactory.javaStringObjectInspector);

        StructObjectInspector argOIs = new TestStandardStructObjectInspector(structFieldNames, structFieldObjectInspectors);
        argOIs.getAllStructFieldRefs();
        userAgentUDTF.initialize(argOIs);
    }

    @Test(expected = UDFArgumentException.class)
    public void initialize_moreThanOneArgument_shouldThrowException() throws UDFArgumentException {

        UserAgentUDTF userAgentUDTF = new UserAgentUDTF();
        List<String> structFieldNames = Arrays.asList(String.valueOf("Name1"), String.valueOf("Name2"));
        List<ObjectInspector> structFieldObjectInspectors =
                Arrays.asList(PrimitiveObjectInspectorFactory.javaStringObjectInspector, PrimitiveObjectInspectorFactory.javaStringObjectInspector);

        StructObjectInspector argOIs = new TestStandardStructObjectInspector(structFieldNames, structFieldObjectInspectors);
        argOIs.getAllStructFieldRefs();
        userAgentUDTF.initialize(argOIs);
    }

    @Test(expected = ClassCastException.class)
    public void initialize_wrongArgumentType_shouldThrowException() throws UDFArgumentException {

        UserAgentUDTF userAgentUDTF = new UserAgentUDTF();
        List<String> structFieldNames = Arrays.asList(String.valueOf("Name1"));
        List<ObjectInspector> structFieldObjectInspectors =
                Arrays.asList(new ThriftUnionObjectInspector());

        StructObjectInspector argOIs = new TestStandardStructObjectInspector(structFieldNames, structFieldObjectInspectors);
        argOIs.getAllStructFieldRefs();
        userAgentUDTF.initialize(argOIs);
    }

    @Test
    public void process_shouldProcessOk() throws Exception {

        JavaStringObjectInspector[] objects = new JavaStringObjectInspector[]{PrimitiveObjectInspectorFactory.javaStringObjectInspector};
        objects[0].create("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1222.36.212.*");
        Field field = userAgentUDTF.getClass().getDeclaredField("stringOI");
        field.setAccessible(true);
        field.set(userAgentUDTF, PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        userAgentUDTF.process(objects);

        verify(collector).collect(any());
    }

    @Test
    public void close_shouldProcessOk() throws HiveException {
        UserAgentUDTF userAgentUDTF = new UserAgentUDTF();
        userAgentUDTF.close();
    }
}