package com.epam;

import eu.bitwalker.useragentutils.UserAgent;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDTF;
import org.apache.hadoop.hive.serde2.objectinspector.*;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Description(name = "UserAgentUDTF",
        value = "_FUNC_(value) - Returns parsed agent value as device, os, browser",
        extended = "Example:\n"
                + " > SELECT _FUNC_('Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)') " +
                "FROM some_table LIMIT 1;\n " +
                "Should return Computer, Windows 7, Internet explorer \n")
public class UserAgentUDTF extends GenericUDTF {

    private PrimitiveObjectInspector stringOI = null;

    /**
     * Method describes what to parse,
     * it contains several validations and define fields (device, os, browser)
     *
     * @param argOIs
     * @return
     * @throws UDFArgumentException
     */
    @Override
    public StructObjectInspector initialize(StructObjectInspector argOIs) throws UDFArgumentException {

        List<? extends StructField> inputFields = argOIs.getAllStructFieldRefs();
        if (argOIs.getAllStructFieldRefs().size() != 1)
            throw new UDFArgumentException("Exactly one argument");

        ObjectInspector UDTFInputOI = inputFields.get(0).getFieldObjectInspector();
        if (UDTFInputOI.getCategory() != ObjectInspector.Category.PRIMITIVE
                && ((PrimitiveObjectInspector) UDTFInputOI).getPrimitiveCategory() != PrimitiveObjectInspector.PrimitiveCategory.STRING)
            throw new UDFArgumentException("Takes a string as a parameter");

        stringOI = (PrimitiveObjectInspector) UDTFInputOI;
        List<String> fieldNames = new ArrayList<>(Arrays.asList("device", "os", "browser"));
        List<ObjectInspector> fieldIOs = new ArrayList<>(3);

        for (int i = 0; i < 3; i++) {
            fieldIOs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        }

        return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, fieldIOs);
    }

    /**
     * Method to process string value.
     * It takes user agent string line and
     * with using useragentutils parse it to 3 objects:
     * - device
     * - operating system
     * - browser name
     *
     * @param objects
     * @throws HiveException
     */
    @Override
    public void process(Object[] objects) throws HiveException {
        
        final String userAgentString = stringOI.getPrimitiveJavaObject(objects[0]).toString();
        UserAgent userAgent = UserAgent.parseUserAgentString(userAgentString);
        Object[] r = new Object[]{
                userAgent.getOperatingSystem().getDeviceType().getName(),
                userAgent.getOperatingSystem().getName(),
                userAgent.getBrowser().getName()
        };
        forward(r);
    }

    /**
     * Due to GenericUDTF we have to override this method,
     * but it is not necessary to implement it in our case
     */
    @Override
    public void close() {
        //do nothing
    }

}
