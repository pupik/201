# Hive custom UDTF

These app is responsible for operating of custom Hive UDTF to parse UserAgent data.

## Prepare for usage
Build app into fat jar with "shadowJar".
Put built jar into necessary path and open hive CLI:
> add jar udf-1.0-SNAPSHOT-all.jar;
> create temporary function user_agent_parse as 'com.epam.UserAgentUDTF';

## Example of usage
After installation of jar to hive classpth and register function, the function could be checked:
> SELECT user_agent_parse('Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');