# Build the Application
For gradle simply execute the clean and build tasks.
 ./gradlew clean build

To skip existing tests if any:
./gradlew clean build -x test

Below listing shows files after a succesfull gradle build.
-distr/target/distr/client-0.1.0.jar
-distr/target/distr/appmaster-0.1.0.jar
-distr/target/distr/container-0.1.0.jar

## Run the Application
To accomplish this, simply run your executable client jar from the projects root dirctory.
java -jar distr/target/distr/client-0.1.0.jar