package com.epam;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.Ordering;
import com.opencsv.CSVReader;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.yarn.annotation.OnContainerStart;
import org.springframework.yarn.annotation.YarnComponent;

import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

@YarnComponent
public class HotelSearchTop3YarnJob {

    private static final Log log = LogFactory.getLog(HotelSearchTop3YarnJob.class);

    private static String FILE_HDFS_PATH = "/Expedia/train.csv";
    private static String SRCH_ADULTS_CNT = "srch_adults_cnt";
    private static String IS_BOOKING = "is_booking";
    private static String HOTEL_CONTINENT = "hotel_continent";
    private static String HOTEL_COUNTRY = "hotel_country";
    private static String HOTEL_MARKET = "hotel_market";
    private static int TOP_3 = 3;

    @Autowired
    private Configuration configuration;

    @OnContainerStart
    public void initAndStartJob() throws Exception {

        CSVReader reader =
                new CSVReader(new FileReader(FILE_HDFS_PATH));

        // read csv file to parquet, to define structure
        String[] columnHeaders = reader.readNext();// first row of csv should contain column headers
        String schemaDesign = generateParquetSchemaDesign(columnHeaders);// generate string scheme
        Schema.Parser parser = new Schema.Parser().setValidate(true);// to validate during parsing, that schema is valid
        Schema parquetSchema = parser.parse(schemaDesign);

        String[] nextLine;
        GenericData.Record record = null;
        HashMap<CompositeKey, Long> hotelCount = new HashMap<>();

        // read line by line and collect neccessary data
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] is an array of values from the line
            record = new GenericData.Record(parquetSchema);
            for (int i = 0; i < columnHeaders.length; i++) {
                record.put(columnHeaders[i], nextLine[i]);
            }

            // Couple defined as SRCH_ADULTS_CNT > 2 and booked hotel define as IS_BOOKING=1
            // Treat hotel as composite key of continent country and market
            if (Integer.parseInt(record.get(SRCH_ADULTS_CNT).toString()) > 2 &&
                    Integer.parseInt(record.get(IS_BOOKING).toString()) == 1) {

                CompositeKey compositeKey =
                        new CompositeKey(record.get(HOTEL_CONTINENT), record.get(HOTEL_COUNTRY), record.get(HOTEL_MARKET));
                Long counter = hotelCount.getOrDefault(compositeKey, 0l);
                hotelCount.put(compositeKey, ++counter);
            }
        }

        printResult(hotelCount);
    }

    /**
     * Print top 3 items
     *
     * @param hotelCount
     */
    public void printResult(HashMap<CompositeKey, Long> hotelCount) {
        List<CompositeKey> keys = getTopKeys(hotelCount, TOP_3);
        keys.forEach(key -> log.info(key + ": " + hotelCount.get(key)));
    }

    /**
     * Method, that generates parquet format design from the column headers
     * <p>
     * It follows the following format
     * {
     * "name": "name of the scheme",
     * "type": "record",
     * "fields": [array of fields, depenfing on column headers]
     * }
     *
     * @param columnHeaders
     * @return String object, that contains design schema, based on headers
     */
    public String generateParquetSchemaDesign(String[] columnHeaders) {
        if (columnHeaders == null || columnHeaders.length < 1) {
            throw new IllegalArgumentException("There is no column headers, please check csv file");
        }

        StringJoiner fieldsJoiner = new StringJoiner(",");

        for (String header : columnHeaders) {
            if (!header.isEmpty())
                fieldsJoiner.add("{\"name\": \"" + header + "\",  \"type\": [\"string\", \"null\"]}");
        }

        String schema = "{\"name\": \"record\"," //Not used in Parquet, can put anything, need for avro engine
                + "\"type\": \"record\"," //Must be set as record
                + "\"fields\": ["
                + fieldsJoiner.toString()
                + " ]}";
        return schema;
    }

    /**
     * get top n keys from map
     *
     * @param map
     * @param n
     * @return
     */
    public <K, V extends Comparable<?>> List<K> getTopKeys(Map<K, V> map, int n) {
        Function<K, V> getVal = Functions.forMap(map);
        Ordering<K> ordering = Ordering.natural().onResultOf(getVal);
        return ordering.greatestOf(map.keySet(), n);
    }
}
