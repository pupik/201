package com.epam;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FsShell;
import org.apache.hadoop.tracing.SpanReceiverHost;
import org.apache.hadoop.util.ToolRunner;
import org.apache.htrace.Sampler;
import org.apache.htrace.Trace;
import org.apache.htrace.TraceScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.yarn.annotation.OnContainerStart;
import org.springframework.yarn.annotation.YarnComponent;

/**
 * Example of job, that could use Zipkin and htrace to trace hadoop components
 */
@YarnComponent
public class TraceFsYarnJob {
    private static final Log log = LogFactory.getLog(TraceFsYarnJob.class);

    @Autowired
    private Configuration configuration;

    @OnContainerStart
    public void initAndStartJob() throws Exception {
        log.info("START TRACE FS");

        configuration.set("spanreceiver.classes", "org.apache.htrace.impl.ZipkinSpanReceiver");
        configuration.set("sampler.classes", "AlwaysSampler");
        configuration.set("zipkin.collector-hostname", "172.18.0.4");
        configuration.set("zipkin.collector-port", "9410");
        configuration.setQuietMode(false);

        FsShell shell = new FsShell();
        shell.setConf(configuration);
        SpanReceiverHost host = SpanReceiverHost.get(configuration, "");
        host.loadSpanReceivers(configuration);

        TraceScope ts = null;
        String[] argv = new String[]{"-ls"};
        try {
            ts = Trace.startSpan("FsShell", Sampler.ALWAYS);
            ToolRunner.run(shell, argv);
        } finally {
            shell.close();
            if (ts != null) ts.close();
        }
        log.info("END TRACE FS ");
    }

}
