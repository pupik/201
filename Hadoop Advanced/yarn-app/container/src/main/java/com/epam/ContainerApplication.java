package com.epam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
public class ContainerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContainerApplication.class, args);
    }

    @Bean
    public HotelSearchTop3YarnJob yarnJob() {
        return new HotelSearchTop3YarnJob();
    }

    @Bean
    public TraceFsYarnJob traceFsJob() {
        return new TraceFsYarnJob();
    }
}
