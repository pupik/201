package com.epam;

import java.util.Arrays;

/**
 * Composite key for hashmap
 */
public class CompositeKey {
    public Object[] keys;

    public CompositeKey(Object... keys) {
        this.keys = keys;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof CompositeKey))
            return false;
        CompositeKey ref = (CompositeKey) obj;

        return Arrays.equals(ref.keys, this.keys);
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
        for (Object key : this.keys) {
            hashCode ^= key.hashCode();
        }
        return hashCode;
    }

    @Override
    public String toString() {
        return Arrays.toString(this.keys);
    }
}
