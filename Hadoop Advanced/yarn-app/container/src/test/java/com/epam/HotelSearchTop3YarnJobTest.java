package com.epam;

import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.stream.LongStream;

public class HotelSearchTop3YarnJobTest {

    @Test(expected = FileNotFoundException.class)
    public void initAndStartJob_wrongFileNameShouldThrowException() throws Exception {
        HotelSearchTop3YarnJob job = new HotelSearchTop3YarnJob();
        Field field = HotelSearchTop3YarnJob.class.getDeclaredField("FILE_HDFS_PATH");
        field.setAccessible(true);
        field.set(null, "someRoot.csv");

        job.initAndStartJob();
    }

    @Test(expected = IllegalArgumentException.class)
    public void generateParquetSchemaDesign_nullHeadersShouldThrowException() {
        HotelSearchTop3YarnJob job = new HotelSearchTop3YarnJob();
        String[] headers = null;
        job.generateParquetSchemaDesign(headers);
    }

    @Test
    public void generateParquetSchemaDesign_shouldProcessOk() {
        HotelSearchTop3YarnJob job = new HotelSearchTop3YarnJob();
        String[] headers = new String[]{"col1", "col2"};
        String result = job.generateParquetSchemaDesign(headers);
        Assert.assertEquals("{\"name\": \"record\"," +
                "\"type\": \"record\"," +
                "\"fields\": [" +
                "{\"name\": \"col1\",  \"type\": [\"string\", \"null\"]}," +
                "{\"name\": \"col2\",  \"type\": [\"string\", \"null\"]} " +
                "]}", result);
    }

    @Test
    public void getTopKeys_shouldProcessOk() {
        HotelSearchTop3YarnJob job = new HotelSearchTop3YarnJob();
        HashMap<String, Long> someMap = new HashMap<>();
        LongStream.range(0, 10).forEach(value ->
            someMap.put("k" + value,value));
        List<String> top2Keys = job.getTopKeys(someMap, 2);
        Assert.assertTrue(top2Keys.contains("k8") && top2Keys.contains("k9"));
    }
}