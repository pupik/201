package com.epam;

import com.opencsv.CSVWriter;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;

public class HotelSearchTop3YarnJobIT {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void startAndInit_shouldProcessOk() throws Exception {
        HotelSearchTop3YarnJob job = new HotelSearchTop3YarnJob();
        Field field = HotelSearchTop3YarnJob.class.getDeclaredField("FILE_HDFS_PATH");
        field.setAccessible(true);
        field.set(null, folder.getRoot().getAbsolutePath() + "/Expedia/train.csv");

        folder.newFolder("Expedia").createNewFile();
        File file = folder.newFile("train.csv");

        generateTestCSV(folder.getRoot().getAbsolutePath() + "/Expedia/train.csv");
        job.initAndStartJob();
    }

    private void generateTestCSV(String fileName) {
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(fileName), ',');
            String[] entries = "srch_adults_cnt#is_booking#hotel_continent#hotel_country#hotel_market".split("#");
            String[] entries2 = "1#1#1#1#1".split("#");
            writer.writeNext(entries);
            writer.writeNext(entries2);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}