#Performance testing
docker run \
    --net host \
    --name yandex_tank \
    -it \
    -v $(pwd):/var/loadtest \
    -v $SSH_AUTH_SOCK:/ssh-agent -e SSH_AUTH_SOCK=/ssh-agent \
    --entrypoint /bin/bash \
    direvius/yandex-tank
