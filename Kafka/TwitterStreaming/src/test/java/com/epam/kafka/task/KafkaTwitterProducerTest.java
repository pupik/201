package com.epam.kafka.task;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import twitter4j.FilterQuery;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

import static org.junit.Assert.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({
        KafkaTwitterProducer.class,
        ConfigurationBuilder.class,
        TwitterStreamFactory.class,
        KafkaProducer.class,
        KafkaTwitterProducer.KafkaStatusListener.class
})
public class KafkaTwitterProducerTest {

    private KafkaTwitterProducer kafkaTwitterProducer;

    @Test
    public void start() throws Exception{
        String consumerKey = "key";//args[0].toString();
        String consumerSecret = "secret";//args[1].toString();
        String accessToken = "token";//args[2].toString();
        String accessTokenSecret = "tokenSecret";//args[3].toString();
        String kafkaHostAndPort = "hostAndPort";//args[4].toString();
        String topicName = "topic";//args[5].toString();
        String[] keyWords = new String[]{"big data", "ai", "machine learning course"};//Arrays.copyOfRange(arguments, 5, arguments.length);

        kafkaTwitterProducer = KafkaTwitterProducer
                .builder()
                .consumerKey(consumerKey)
                .consumerSecret(consumerSecret)
                .accessToken(accessToken)
                .accessTokenSecret(accessTokenSecret)
                .kafkaHostAndPort(kafkaHostAndPort)
                .topicName(topicName)
                .keyWords(keyWords)
                .build();

        assertEquals(consumerKey, kafkaTwitterProducer.getConsumerKey());
        assertEquals(consumerSecret, kafkaTwitterProducer.getConsumerSecret());
        assertEquals(accessToken, kafkaTwitterProducer.getAccessToken());
        assertEquals(accessTokenSecret, kafkaTwitterProducer.getAccessTokenSecret());
        assertEquals(kafkaHostAndPort, kafkaTwitterProducer.getKafkaHostAndPort());
        assertEquals(topicName, kafkaTwitterProducer.getTopicName());
        assertArrayEquals(keyWords, kafkaTwitterProducer.getKeyWords());

        TwitterStreamFactory mockTwitterStreamFactory = PowerMockito.mock(TwitterStreamFactory.class);
        PowerMockito.whenNew(TwitterStreamFactory.class).withAnyArguments().thenReturn(mockTwitterStreamFactory);

        TwitterStream mockTwitterStream = PowerMockito.mock(TwitterStream.class);
        Mockito.when(mockTwitterStreamFactory.getInstance()).thenReturn(mockTwitterStream);

        KafkaProducer mockKafkaProducer = PowerMockito.mock(KafkaProducer.class);
        PowerMockito.whenNew(KafkaProducer.class).withAnyArguments().thenReturn(mockKafkaProducer);

        KafkaTwitterProducer.KafkaStatusListener listener = PowerMockito.mock(KafkaTwitterProducer.KafkaStatusListener.class);
        PowerMockito.whenNew(KafkaTwitterProducer.KafkaStatusListener.class).withAnyArguments().thenReturn(listener);

        kafkaTwitterProducer.start();

        Mockito.verify(mockTwitterStreamFactory).getInstance();
        Mockito.verify(mockTwitterStream).filter(Mockito.any(FilterQuery.class));
    }
}