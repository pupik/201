package com.epam.kafka.task;

import java.util.Arrays;

/**
 * Entry point for execution of Kafka Twitter Producer
 *
 * At the moment of development, these params were suitable
 *  - consumerKey = "jmNrxfuTzspBmrBf9qSEnmp9w";
 *  - consumerSecret = "qnI7MTufqtKcwIMDlGKRxbySWEO8Q3GjuopnfMn2t0SSeNvj7x";
 *  - accessToken = "1091327668262453248-ACpYxysAvMk9URdquysYDS72oMfnTy";
 *  - accessTokenSecret = "jJcUksP5QSP0kCNprbjczoqVRGjmtlW8pGFT9Mkl5rWn8";
 *  - kafkaHostAndPort = "localhost:9092";
 *  - topicName = "kafka-task";
 *  - keyWords = new String[]{"big data", "ai", "machine learning course"};
 */
public class Main {

    public static void main(String[] args) {
        if (args.length < 6) {
            System.out.println("Usage: KafkaTwitterProducer <twitter-consumer-key> <twitter-consumer-secret> " +
                    "<twitter-access-token> <twitter-access-token-secret> <kafka-host:post> <topic-name> <twitter-search-keywords>");
            return;
        }

        String consumerKey = args[0];
        String consumerSecret = args[1];
        String accessToken = args[2];
        String accessTokenSecret = args[3];
        String kafkaHostAndPort = args[4];
        String topicName = args[5];
        String[] keyWords = Arrays.copyOfRange(args, 6, args.length);

        KafkaTwitterProducer producer = KafkaTwitterProducer
                .builder()
                .consumerKey(consumerKey)
                .consumerSecret(consumerSecret)
                .accessToken(accessToken)
                .accessTokenSecret(accessTokenSecret)
                .kafkaHostAndPort(kafkaHostAndPort)
                .topicName(topicName)
                .keyWords(keyWords)
                .build();
        producer.start();
    }
}
