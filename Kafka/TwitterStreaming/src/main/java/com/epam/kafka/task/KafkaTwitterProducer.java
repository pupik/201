package com.epam.kafka.task;

import lombok.Builder;
import lombok.Data;
import org.apache.hive.jdbc.HivePreparedStatement;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import twitter4j.*;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import java.util.Properties;

/**
 * A Kafka Producer that gets tweets on certain keywords
 * from twitter datasource and publishes to a kafka topic
 * <p>
 * Arguments: <consumerKey> <consumerSecret> <accessToken> <accessTokenSecret> <kafkaHost:port> <topic-name> <keyword_1> ... <keyword_n>
 * <consumerKey>	- Twitter consumer key
 * <consumerSecret>  	- Twitter consumer secret
 * <accessToken>	- Twitter access token
 * <accessTokenSecret>	- Twitter access token secret
 * <kafkaHost:post>	- Twitter access token secret
 * <topic-name>		- The kafka topic to subscribe to
 * <keyword_1>		- The keyword to filter tweets
 * <keyword_n>		- Any number of keywords to filter tweets
 */
@Data
@Builder
public class KafkaTwitterProducer {
    private TwitterStream twitterStream;
    private Producer<String, String> producer;

    private String consumerKey;
    private String consumerSecret;
    private String accessToken;
    private String accessTokenSecret;
    private String kafkaHostAndPort;
    private String topicName;
    private String[] keyWords;

    public void start() {
        // Set twitter oAuth tokens in the configuration
        ConfigurationBuilder builder = new ConfigurationBuilder();
        Configuration configuration = builder
                .setDebugEnabled(true)
                .setJSONStoreEnabled(true)
                .setOAuthConsumerKey(consumerKey)
                .setOAuthConsumerSecret(consumerSecret)
                .setOAuthAccessToken(accessToken)
                .setOAuthAccessTokenSecret(accessTokenSecret).build();

        // Create twitter stream using the configuration
        twitterStream = new TwitterStreamFactory(configuration).getInstance();

        // Filter keywords
        FilterQuery query = new FilterQuery()
                .language("en")
                .track(keyWords);

        // Add Kafka producer config settings
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaHostAndPort);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        producer = new KafkaProducer<>(props);

        HivePreparedStatement asd = null;

        try {
            System.out.println("Parse");
            Status status =
                    (Status) TwitterObjectFactory
                            .createObject("{\"text\":\"Some text\",\"created_at\":\"Thu Feb 21 15:13:12 +0000 2019\",\"place\":{\"country_code\":\"KZ\"},\"entities\":{\"hashtags\":[{\"text\":\"ABR\",\"indices\":[105,116]}]}}");

            producer.send(new ProducerRecord<>(
                    topicName,
                    "TEST",
                    TwitterObjectFactory.getRawJSON(status)));


//            status =
//                    (Status) TwitterObjectFactory
//                            .createObject("{\"text\":\"Some text\",\"created_at\":\"Fri Feb 22 15:13:12 +0000 2019\",\"place\":{\"country_code\":\"US\"},\"entities\":{\"hashtags\":[{\"text\":\"DupDupApp\",\"indices\":[105,116]}]}}");
//            producer.send(new ProducerRecord<>(
//                    topicName,
//                    "TEST",
//                    TwitterObjectFactory.getRawJSON(status)));
//
//
//            status =
//                    (Status) TwitterObjectFactory
//                            .createObject("{\"text\":\"Some text\",\"created_at\":\"Sat Feb 23 15:13:12 +0000 2019\",\"place\":{\"country_code\":\"US\"},\"entities\":{\"hashtags\":[{\"text\":\"DupDupApp\",\"indices\":[105,116]}]}}");
//
//            producer.send(new ProducerRecord<>(
//                    topicName,
//                    "TEST",
//                    TwitterObjectFactory.getRawJSON(status)));
//
//
//            status =
//                    (Status) TwitterObjectFactory
//                            .createObject("{\"text\":\"Some text\",\"created_at\":\"Sun Feb 24 15:13:12 +0000 2019\",\"place\":{\"country_code\":\"US\"},\"entities\":{\"hashtags\":[{\"text\":\"DupDupApp\",\"indices\":[105,116]}]}}");
//
//            producer.send(new ProducerRecord<>(
//                    topicName,
//                    "TEST",
//                    TwitterObjectFactory.getRawJSON(status)));


            //
            status =
                    (Status) TwitterObjectFactory
                            .createObject("{\"text\":\"Some text\",\"created_at\":\"Thu Feb 21 15:13:12 +0000 2019\",\"place\":{\"country_code\":\"BE\"},\"entities\":{\"hashtags\":[{\"text\":\"DupDupApp\",\"indices\":[105,116]}]}}");

            producer.send(new ProducerRecord<>(
                    topicName,
                    "TEST",
                    TwitterObjectFactory.getRawJSON(status)));


//            status =
//                    (Status) TwitterObjectFactory
//                            .createObject("{\"text\":\"Some text\",\"created_at\":\"Fri Feb 22 15:13:12 +0000 2019\",\"place\":{\"country_code\":\"BE\"},\"entities\":{\"hashtags\":[{\"text\":\"DupDupApp\",\"indices\":[105,116]}]}}");
//            producer.send(new ProducerRecord<>(
//                    topicName,
//                    "TEST",
//                    TwitterObjectFactory.getRawJSON(status)));
//
//
//            status =
//                    (Status) TwitterObjectFactory
//                            .createObject("{\"text\":\"Some text\",\"created_at\":\"Sat Feb 23 15:13:12 +0000 2019\",\"place\":{\"country_code\":\"BE\"},\"entities\":{\"hashtags\":[{\"text\":\"DupDupApp\",\"indices\":[105,116]}]}}");
//
//            producer.send(new ProducerRecord<>(
//                    topicName,
//                    "TEST",
//                    TwitterObjectFactory.getRawJSON(status)));
//
//
//            status =
//                    (Status) TwitterObjectFactory
//                            .createObject("{\"text\":\"Some text\",\"created_at\":\"Sun Feb 24 15:13:12 +0000 2019\",\"place\":{\"country_code\":\"BE\"},\"entities\":{\"hashtags\":[{\"text\":\"DupDupApp\",\"indices\":[105,116]}]}}");
//            producer.send(new ProducerRecord<>(
//                    topicName,
//                    "TEST",
//                    TwitterObjectFactory.getRawJSON(status)));


            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println("Error bitch");
            e.printStackTrace();
        }

//        StatusListener listener = new KafkaStatusListener();
//        twitterStream.addListener(listener);
//        twitterStream.filter(query);
    }

    class KafkaStatusListener implements StatusListener {

        @Override
        public void onStatus(Status status) {
            // i.e. means, that only items with Place entity and Belarus code should be added
            if (status.getPlace() != null) {
                System.out.println(status);
                producer.send(new ProducerRecord<>(
                        topicName,
                        status.getUser().getName(),
                        TwitterObjectFactory.getRawJSON(status)));
            }
        }

        @Override
        public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
        }

        @Override
        public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
        }

        @Override
        public void onScrubGeo(long userId, long upToStatusId) {
        }

        @Override
        public void onStallWarning(StallWarning warning) {
        }

        @Override
        public void onException(Exception ex) {
            ex.printStackTrace();
            twitterStream.shutdown();
        }
    }
}