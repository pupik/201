# KafkaTwitterProducer

These app is responsible for taking some data from twitter filtered by kewords and publishing them into kafka topic

## Usage
Build app into jar with "fatJar" and use it in a way:

java -jar app.jar <twitter-consumer-key> <twitter-consumer-secret> <twitter-access-token> <twitter-access-token-secret> <kafka-host:post> <topic-name> <twitter-search-keywords>

## Example:
java -jar twitter-app-1.0-SNAPSHOT-all.jar jmNrxfuTzspBmrBf9qSEnmp9w qnI7MTufqtKcwIMDlGKRxbySWEO8Q3GjuopnfMn2t0SSeNvj7x 1091327668262453248-ACpYxysAvMk9URdquysYDS72oMfnTy jJcUksP5QSP0kCNprbjczoqVRGjmtlW8pGFT9Mkl5rWn8 localhost:9092 homework "big data" "ai" "machine learning course"